/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.algo;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Arthi
 */
public class MaxSub {

    static long start, stop, I = 0;

    public static void main(String[] args) {
        start = System.nanoTime();
        stop = System.nanoTime();
        int[] A = new int[10000];
        int[] B = {1, 5, -8, 10, 2, -1, 2};
        int[] C = {-10,5,-2,5,7};
        int[] D = new int[100000];
        for (int i = 0; i < 10000; i++) {
            A[i] = i;
        }
        ag3(D);
        ag2(D);
        ag1(D);
    }

    public static void ag1(int[] A) { //MaxSubSlow
        start = System.nanoTime();
        long m = 0;
        for (int j = 0; j < A.length; j++) {
            for (int k = j; k < A.length; k++) {
                int s = 0;
                for (int i = j; i <= k; i++) {
                    s += A[i];
                }
                if (s > m) {
                    m = s;
                }
            }
        }
        System.out.println("algor1: " + m);
        stop = System.nanoTime();
        System.out.println("algor1 Time: " + (stop - start) * 1E-9 + "secs.");
         System.out.println();
    }

    public static void ag2(int[] A) { 
        start = System.nanoTime();
        ArrayList<Integer> S = new ArrayList<Integer>();
        ArrayList<Integer> R = new ArrayList<Integer>();
        for (int i = 0; i < A.length; i++) {
            R.add(A[i]);
        }
        R.add(0, 0);
        for (int i = 0; i < A.length; i++) {
            if (i == 0) {
                S.add(i, 0 + A[i]);
            } else {
                S.add(i, S.get(i - 1) + A[i]);
            }
        }
        int m = 0;
        int s = 0;
        for (int j = 0; j < S.size(); j++) {
            for (int k = j; k < S.size(); k++) {
                if (j == 0) {
                    s = S.get(k) + 0;
                } else {
                    s = S.get(k) - S.get(j - 1);
                }
                if (s > m) {
                    m = s;
                }
            }
        }
        System.out.println("algor2: " + m);
        stop = System.nanoTime();
        System.out.println("algor2 Time: " + (stop - start) * 1E-9 + "secs.");
        System.out.println();
    }

    public static void ag3(int[] A) {
        start = System.nanoTime();
        ArrayList<Integer> M = new ArrayList<Integer>();
        for (int t = 0; t < A.length; t++) {
            if(t==0){
                M.add(t,Math.max(0, A[t])); 
            }else{
                M.add(t,Math.max(0, M.get(t-1)+A[t]));
            }
        }
        int m =0;
        for (int t = 0; t < M.size(); t++) {
            m=Math.max(m, M.get(t));
        }
        System.out.println("algor3: " + m);
        stop = System.nanoTime();
        System.out.println("algor3 Time: " + (stop - start) * 1E-9 + "secs.");
        System.out.println();
    }
}
