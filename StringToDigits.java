/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.algo;

import java.util.Scanner;

/**
 *
 * @author Arthi
 */
public class StringToDigits {

    public static void main(String[] args) {
        String input = (new Scanner(System.in)).next();
        System.out.println(StringToDigit(input));
    }

    private static int StringToDigit(String A) {
        int sum = 0; // ผลรวม
        int valueOfPosition  = 1; //ตำแหน่งเริ่มตั้งแต่หลักหน่วย 
        for (int i = A.length() - 1; i >= 0; i--) {
            if (A.charAt(i) == '1') {
                sum += 1 * valueOfPosition ;
            } else if (A.charAt(i) == '2') {
                sum += 2 * valueOfPosition ;
            } else if (A.charAt(i) == '3') {
                sum += 3 * valueOfPosition ;
            } else if (A.charAt(i) == '4') {
                sum += 4 * valueOfPosition ;
            } else if (A.charAt(i) == '5') {
                sum += 5 * valueOfPosition ;
            } else if (A.charAt(i) == '6') {
                sum += 6 * valueOfPosition ;
            } else if (A.charAt(i) == '7') {
                sum += 7 * valueOfPosition ;
            } else if (A.charAt(i) == '8') {
                sum += 8 * valueOfPosition ;
            } else if (A.charAt(i) == '9') {
                sum += 9 * valueOfPosition ;
            } else if (i == 0 && A.charAt(i) == '-') { //เช็คกรณีที่มีค่าเป็นลบ
                sum *= -1;
            }
            valueOfPosition  = valueOfPosition  * 10; //เพิ่มตำแหน่งหลักขึ้นไปเรื่อยๆ
        }
        return sum;
    }
}
